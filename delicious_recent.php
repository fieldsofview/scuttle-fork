<?php
	require_once "header.inc.php";
	$bookmarkservice =& ServiceFactory::getServiceInstance('BookmarkService');
	$templateservice =& ServiceFactory::getServiceInstance('TemplateService');
	$userservice     =& ServiceFactory::getServiceInstance('UserService');
	$cacheservice    =& ServiceFactory::getServiceInstance('CacheService');
	
	$uId = $userservice->getCurrentUserId();
	$userDet=$userservice->getUser($uId);
	$del_user=$userDet['del_user'];
	if(strlen($del_user)==0 or $del_user===" "){
		echo "You need to set a delicious user to sync with delicious.";
		die;
	}
	
	$del_password=mcrypt_decrypt(MCRYPT_RIJNDAEL_256,"scuttle_key_123",$userDet['del_password'],MCRYPT_MODE_CFB);
	//href hash description tag time extended meta 
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.del.icio.us/v1/posts/recent?");
	curl_setopt($ch,CURLOPT_USERPWD,"$del_user:$del_password"); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$output = curl_exec($ch);
	curl_close($ch); 
	$xml=simplexml_load_string($output);
	$count=0;
	foreach($xml->children() as $child)
	{
		$status=0;
		foreach($child->attributes() as $attr => $value)
		{
			//echo "$attr ";
			if($attr=="href")
			{
				$href=$value;
			}
			if($attr=="description")
			{
				$desc=$value;
			}
			if($attr=="tag")
			{
				$tag=$value;
			}
			if($attr=="time")
			{
				$time=$value;
			}
			if($attr=="shared" and $value=="no")
			{
				$status=2;
			}
		}
		if(!$bookmarkservice->bookmarkExists($href))
		{
			if(isset($_GET['confirm']))
			{
				//addBookmark($address, $title, $description, $status, $categories, $date = NULL, $fromApi = false, $fromImport = false) 
				$time=str_replace("T"," ",$time);
				$time=str_replace("Z","",$time);
				$tag=str_replace(" ",",",$tag);
				//echo "Adding $desc<br/>";
				$bookmarkservice->addBookmarkWithoutDelicious(urldecode(trim(urlencode($href))),trim($desc),NULL,$status,trim($tag),$time);
				$count++;
				//echo "<br/>";
			}
			if(!isset($_GET['confirm'])){
				$count++;
			}
		}
		//echo "$href $desc $tag<br/>";
	
	}
	if(isset($_GET['confirm'])){
		echo "Done! $count bookmarks added to your account. Click <a href='../'>here</a> to go back.";
	}else{
		if($count==0){
			echo "Your account is synced with delicious. Click <a href='../'>here</a> to go back.";
		}else{
                        $noURL=createURL('bookmarks');
			echo "You have $count new booksmarks to be synced with your scuttle account. Would you like to continue? <br/><a href='delicious_sync.php?confirm=yes'>Yes</a><br/><a href='$noURL'>No</a>";
		}
	}
	
?>
