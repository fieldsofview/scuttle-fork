<?php
include_once "../config.inc.php";
$this->includeTemplate($GLOBALS['top_include']);
?>
<h3>Import directly from delicious</h3>
<p>
Click <a href="<?php echo createURL('delicious_recent');?>">here</a> to sync <strong>only your most recent delicious bookmarks</strong>.
</p>

<p>
Click <a href= "<?php echo createURL('delicious_sync');?>">here</a> to sync with delicious using the delicious API. You need to have stored delicious username and password in scuttle for this to work. <strong>Please use this sparingly, preferably only when sync-ing with scuttle for the first time.<strong>
</p>
<h3>Import from file</h3>
<form id="import" enctype="multipart/form-data" action="<?php echo $formaction; ?>" method="post">
<table>
<tr valign="top">
    <th align="left"><?php echo T_('File'); ?></th>
    <td>
        <input type="hidden" name="MAX_FILE_SIZE" value="1024000" />
        <input type="file" name="userfile" size="50" />
    </td>
</tr>
<tr valign="top">
    <th align="left"><?php echo T_('Privacy'); ?></th>
    <td>
        <select name="status">
            <option value="0"><?php echo T_('Public'); ?></option>
            <option value="1"><?php echo T_('Shared with Watchlist'); ?></option>
            <option value="2"><?php echo T_('Private'); ?></option>
        </select>
    </td>
</tr>
<tr>
    <td />
    <td><input type="submit" value="<?php echo T_('Import'); ?>" /></td>
</tr>
</table>
</form>

<h3><?php echo T_('Instructions for file import'); ?></h3>
<ol>
    <li><?php echo T_('Log in to the <a href="http://del.icio.us/api/posts/all">export page at del.icio.us</a>'); ?>.</li>
    <li><?php echo T_('Save the resulting <abbr title="Extensible Markup Language">XML</abbr> file to your computer'); ?>.</li>
    <li><?php echo T_('Click <kbd>Browse...</kbd> to find this file on your computer. The maximum size the file can be is 1MB'); ?>.</li>
    <li><?php echo T_('Select the default privacy setting for your imported bookmarks'); ?>.</li>
    <li><?php echo T_('Click <kbd>Import</kbd> to start importing the bookmarks; it may take a minute'); ?>.</li>
</ol>

<?php
$this->includeTemplate($GLOBALS['bottom_include']);
?>
