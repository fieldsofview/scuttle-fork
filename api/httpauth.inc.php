<?php
//  Provides HTTP Basic authentication of a user, and sets two variables, sId and username,
//  with the user's info.
list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) =
    explode(':' , base64_decode(substr($_SERVER['REDIRECT_REMOTE_USER'], 6)));

function authenticate() {
    header('WWW-Authenticate: Basic realm="API"');
    header('HTTP/1.0 401 Unauthorized');
    die('Use of the API calls requires authentication.');
}

if (!isset($_SERVER['PHP_AUTH_USER'])) {
    authenticate();
} else {
	echo $_SERVER['PHP_AUTH_USER'];
    require_once('../header.inc.php');
    $userservice =& ServiceFactory::getServiceInstance('UserService');

    $login = $userservice->login($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']); 
    if (!$login) {
//		echo "Error";     
 //  authenticate();
    }
}
?>
